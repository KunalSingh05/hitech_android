package com.hitech.kunalsingh.customersupport.fragment_classes;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hitech.kunalsingh.customersupport.R;

/**
 * Created by kunal.singh1 on 15-01-2017.
 */
public class FAQHelpfragment extends Fragment {

    View rootView;
    public FAQHelpfragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_faq_help_screen, container, false);
        // Inflate the layout for this fragment

        return rootView;
    }

}
