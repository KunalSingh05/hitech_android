package com.hitech.kunalsingh.customersupport.home;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.CheckBox;
import android.widget.LinearLayout;

import com.hitech.kunalsingh.customersupport.R;
import com.hitech.kunalsingh.customersupport.common_ui.HiTechButton;
import com.hitech.kunalsingh.customersupport.common_ui.HiTechEditText;
import com.hitech.kunalsingh.customersupport.common_ui.HiTechTextView;
import com.hitech.kunalsingh.customersupport.controller.BaseController;
import com.hitech.kunalsingh.customersupport.fragment_drawer.MainActivity;
import com.hitech.kunalsingh.customersupport.otp.ReceiveOTPScreenActivity;
import com.hitech.kunalsingh.customersupport.parser.LoginData;
import com.hitech.kunalsingh.customersupport.parser.LoginDataParser;
import com.hitech.kunalsingh.customersupport.utility.PrefManager;
import com.hitech.kunalsingh.customersupport.utility.QueryBuilder;
import com.hitech.kunalsingh.customersupport.utility.Settings;
import com.hitech.kunalsingh.customersupport.utility.Utils;

/**
 * Created by kunal.singh1 on 08-01-2017.
 */
public class LoginScreenActivity extends AppCompatActivity implements View.OnClickListener,LoginDataParser.onLoginListener{
    private HiTechEditText edtTxtUserName,edtTxtPassword;
    private HiTechTextView txtViewForgotPassword,txtViewSkip,txtViewRegisterNewUser;
    private CheckBox cbRememberMe;
    private HiTechButton btnLogin;
    private Animation shake;
    private LinearLayout mainLayout;
    LoginDataParser loginParser;
    LoginData loginData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Login");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initialisation();
        setListener();
    }

    private void initialisation() {
        edtTxtUserName= (HiTechEditText) findViewById(R.id.edtTxtUserName);
        edtTxtPassword= (HiTechEditText) findViewById(R.id.edtTxtPassword);
        cbRememberMe = (CheckBox) findViewById(R.id.rememberMe);
        txtViewForgotPassword= (HiTechTextView) findViewById(R.id.txtViewForgotPassword);
        txtViewSkip= (HiTechTextView) findViewById(R.id.txtViewSkip);
        txtViewRegisterNewUser= (HiTechTextView) findViewById(R.id.txtViewRegisterNewUser);
        btnLogin= (HiTechButton) findViewById(R.id.btnLogin);
    }

    private void setListener() {
        btnLogin.setOnClickListener(this);
        txtViewForgotPassword.setOnClickListener(this);
        txtViewSkip.setOnClickListener(this);
        txtViewRegisterNewUser.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.txtViewForgotPassword:
                startActivity(new Intent(LoginScreenActivity.this, ReceiveOTPScreenActivity.class));
                finishAffinity();
                break;
            case R.id.btnLogin:
                if(edtTxtUserName.getText().toString().trim().equalsIgnoreCase("")){
                    edtTxtUserName.setError("Please enter user name.");
                    edtTxtUserName.requestFocus();
                }else if(edtTxtPassword.getText().toString().trim().equalsIgnoreCase("")) {
                    edtTxtPassword.setError("Please enter password.");
                    edtTxtPassword.requestFocus();
                }else {
                    loginParser = new LoginDataParser();
                    loginParser.executeQuery(LoginScreenActivity.this, QueryBuilder.getInstance().login(), QueryBuilder.getInstance().generateLoginQuery(LoginScreenActivity.this, edtTxtUserName.getText().toString().trim(), edtTxtPassword.getText().toString().trim(), "2.2"), LoginScreenActivity.this, Settings.LOGIN_ID, true);
                }
                break;
            case R.id.txtViewSkip:
                startActivity(new Intent(LoginScreenActivity.this, MainActivity.class));
                finishAffinity();
                break;
            case R.id.txtViewRegisterNewUser:
                startActivity(new Intent(LoginScreenActivity.this, RegisterScreenActivity.class));
                finishAffinity();
            default:
                break;
        }
    }

    @Override
    public void onSuccess(int id, String message, Object object) {
        if (id == Settings.LOGIN_ID) {
       /*     if (object != null) {
                loginData = (LoginData) object;
                populateData();
            }
        } else {
            if (message != null)
                Utils.AltToast(this, message);
            finish();
        }*/
            PrefManager.setLogin(this,true);
            Utils.HitechToast(this, message);
            startActivity(new Intent(LoginScreenActivity.this,MainActivity.class));
        }
    }

    @Override
    public void onError(String error, int id, BaseController.ErrorCode code) {
            if (error != null) {
                Utils.HitechToast(this, error);
            }

    }
}
