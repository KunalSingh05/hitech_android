package com.hitech.kunalsingh.customersupport.fragment_drawer;

/**
 * NavDrawerItem
 * Created by Kunal Singh
 * Date 15 January 2017
 */
public class NavDrawerItem {
    private boolean showNotify;
    private String title;
    private int icon;
    private   int selIcon;


    public NavDrawerItem() {

    }

    public NavDrawerItem(boolean showNotify, String title, int icon) {
        this.showNotify = showNotify;
        this.title = title;
        this.icon=icon;
    }

    public boolean isShowNotify() {
        return showNotify;
    }

    public void setShowNotify(boolean showNotify) {
        this.showNotify = showNotify;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    public int getIcon(){
        return icon;
    }

    public void setIcon(int icon){
        this.icon =icon;
    }


    public int getSelIcon(){
        return selIcon;
    }

    public void setSelIcon(int selIcon){
        this.selIcon =selIcon;
    }


}

