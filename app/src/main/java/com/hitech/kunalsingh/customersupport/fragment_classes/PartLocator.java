package com.hitech.kunalsingh.customersupport.fragment_classes;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.hitech.kunalsingh.customersupport.R;
import com.hitech.kunalsingh.customersupport.utility.Settings;

/**
 * Created by kunal.singh1 on 15-01-2017.
 */
public class PartLocator extends Fragment {
    private WebView simpleWebView;
    View rootView;
    ProgressDialog pd;
    public PartLocator() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_part_locator_screen, container, false);
        // Inflate the layout for this fragment
        simpleWebView = (WebView)rootView.findViewById(R.id.simpleWebView);
        simpleWebView.setWebViewClient(new MyWebViewClient());
        pd = new ProgressDialog(getActivity());
        pd.setMessage("Please wait Loading...");
        pd.show();
        simpleWebView.getSettings().setJavaScriptEnabled(true);
        simpleWebView.loadUrl(Settings.PART_LOCATOR); // load a web page in a web view
        return rootView;
    }
    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            if (!pd.isShowing()) {
                pd.show();
            }
            return true;
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);

            if (pd.isShowing()) {
                pd.dismiss();
            }
        }
    }
}
