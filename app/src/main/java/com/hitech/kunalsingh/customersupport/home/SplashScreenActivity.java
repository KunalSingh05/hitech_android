package com.hitech.kunalsingh.customersupport.home;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import com.hitech.kunalsingh.customersupport.R;
import com.hitech.kunalsingh.customersupport.fragment_drawer.MainActivity;
import com.hitech.kunalsingh.customersupport.utility.PrefManager;
import com.hitech.kunalsingh.customersupport.utility.Settings;

/**
 * Splash Screen
 * Created by Kunal Singh
 * Date 29 december 2016
 * */
public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_splash_screen);
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                if(PrefManager.isLogin(SplashScreenActivity.this)==true){
                    startActivity(new Intent(SplashScreenActivity.this,MainActivity.class));
                    finish();
                }else {
                    startActivity(new Intent(SplashScreenActivity.this, LoginScreenActivity.class));
                    // close this activity
                    finish();
                }
            }
        }, Settings.SPLASH_TIME_OUT);
    }
}
