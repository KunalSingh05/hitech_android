package com.hitech.kunalsingh.customersupport.fragment_drawer;

import android.annotation.TargetApi;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.hitech.kunalsingh.customersupport.R;
import com.hitech.kunalsingh.customersupport.fragment_classes.DealerLocator;
import com.hitech.kunalsingh.customersupport.fragment_classes.HomeFragment;
import com.hitech.kunalsingh.customersupport.fragment_classes.AboutHGP;
import com.hitech.kunalsingh.customersupport.fragment_classes.CompanyProfile;
import com.hitech.kunalsingh.customersupport.fragment_classes.PartLocator;
import com.hitech.kunalsingh.customersupport.home.LoginScreenActivity;
import com.hitech.kunalsingh.customersupport.utility.PrefManager;

/**
 * MainActivity For Navigation Fragment
 * Created by Kunal
 * Date 10 April 2016
 */
public class MainActivity extends AppCompatActivity implements FragmentDrawer.FragmentDrawerListener {
    private static String TAG = MainActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private FragmentDrawer drawerFragment;
    TextView tvToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        tvToolbar= (TextView) mToolbar.findViewById(R.id.tvToolbar);
        mToolbar.setTitleTextColor(Color.parseColor("#000000"));
        setSupportActionBar(mToolbar);
        tvToolbar.setText(mToolbar.getTitle());
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        drawerFragment = (FragmentDrawer) getSupportFragmentManager().findFragmentById(R.id.fragment_navigation_drawer);
        drawerFragment.setUp(R.id.fragment_navigation_drawer, (DrawerLayout) findViewById(R.id.drawer_layout), mToolbar);
        drawerFragment.setDrawerListener(this);
        // display the first navigation drawer view on app launch
        displayView(0);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
       // getMenuInflater().inflate(R.menu.main_menu, menu);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onDrawerItemSelected(View view, int position) {
        displayView(position);
    }

    private void displayView(int position) {
        Fragment fragment = null;
        String title = "HGP";
        switch (position) {
            case 0:
                fragment = new HomeFragment();
                title = getString(R.string.title_home);
                break;
            case 1:
                fragment = new CompanyProfile();
                title = getString(R.string.title_company_profile);
                break;

            case 2:
                fragment = new AboutHGP();
                title = getString(R.string.title_about_hgp);
                break;

            case 3:
                fragment=new DealerLocator();
                title =getString(R.string.title_dealer_location);
                break;
            case 4:
                fragment=new PartLocator();
                title =getString(R.string.title_part_locater);
                /*try {
                    Uri uri = Uri.parse("market://details?id=com.hitech.tirth");
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=com.hitech.tirth")));
                }*/
                break;
         case 5:
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Logout");
                builder.setMessage("Are you sure, you want to logout?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PrefManager.clearAll(MainActivity.this);
                        Intent loginIntent = new Intent(MainActivity.this, LoginScreenActivity.class);
                       // Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                        //startActivity(loginIntent,bndlanimation);
                        startActivity(loginIntent);
                        MainActivity.this.finish();
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.show();
                break;
              /* case 6:
                fragment=new AboutFragment();
                title =getString(R.string.title_about);
                break;
            case 7:
                fragment=new ContactusFragment();
                title =getString(R.string.title_contact);
                break;
            case 8:
                fragment=new FAQHelpfragment();
                title =getString(R.string.title_faq);
                break;
            case 9:
                fragment=new TermsConditionFragment();
                title=getString(R.string.title_terms);
                break;
            case 10:
                AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
                builder.setTitle("Logout");
                builder.setMessage("Are you sure, you want to logout?");
                builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent loginIntent = new Intent(MainActivity.this, LoginScreenActivity.class);
                       // Bundle bndlanimation = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.animation, R.anim.animation2).toBundle();
                        //startActivity(loginIntent,bndlanimation);
                        startActivity(loginIntent);
                        MainActivity.this.finish();
                    }
                });
                builder.setNegativeButton("Cancel", null);
                builder.show();*/
            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            //fragmentTransaction.setCustomAnimations(R.anim.animation, R.anim.animation2);
            fragmentTransaction.replace(R.id.container_body, fragment);
            fragmentTransaction.commit();
            // set the toolbar title
           // getSupportActionBar().setTitle(title);
            tvToolbar.setText(title);
        }
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle);
        builder.setTitle("Close");
        builder.setMessage("Are you sure, you want to close application?");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onClick(DialogInterface dialog, int which) {
                MainActivity.this.finishAffinity();
            }
        });
        builder.setNegativeButton("Cancel", null);
        builder.show();
    }

}
