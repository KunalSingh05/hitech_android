package com.hitech.kunalsingh.customersupport.otp;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.hitech.kunalsingh.customersupport.R;
import com.hitech.kunalsingh.customersupport.common_ui.HiTechButton;

/**
 * Receive OTP Screen
 */
public class ReceiveOTPScreenActivity extends AppCompatActivity implements View.OnClickListener {
    private HiTechButton btnVerify;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Verify");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initialisation();
        setListener();
    }
    private void initialisation() {
        btnVerify= (HiTechButton) findViewById(R.id.btnVerify);
    }

    private void setListener() {
        btnVerify.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnVerify:
                startActivity(new Intent(ReceiveOTPScreenActivity.this, ChangePasswordActivity.class));
                finishAffinity();
                break;
            default:
                break;
        }
    }
}
