package com.hitech.kunalsingh.customersupport.otp;

import android.annotation.TargetApi;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.hitech.kunalsingh.customersupport.R;
import com.hitech.kunalsingh.customersupport.common_ui.HiTechButton;
import com.hitech.kunalsingh.customersupport.common_ui.HiTechEditText;
import com.hitech.kunalsingh.customersupport.fragment_drawer.MainActivity;

/**
 * Created by kunal.singh1 on 14-01-2017.
 */
public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {
    private HiTechButton btnDone;
    private HiTechEditText edtTextNewPassword, edtTextConfNewPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password_screen);
        Toolbar toolbar = (Toolbar) findViewById(R.id.header);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Set Password");
        //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        initialisation();
        setListener();

    }

    private void initialisation() {
        edtTextNewPassword = (HiTechEditText) findViewById(R.id.edtTextNewPassword);
        edtTextConfNewPassword = (HiTechEditText) findViewById(R.id.edtTextConfNewPassword);
        btnDone = (HiTechButton) findViewById(R.id.btnDone);
    }

    private void setListener() {
        btnDone.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnDone:
                startActivity(new Intent(ChangePasswordActivity.this, MainActivity.class));
                finishAffinity();
                break;
            default:
                break;
        }
    }
}
