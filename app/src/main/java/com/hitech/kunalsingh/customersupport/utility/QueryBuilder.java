package com.hitech.kunalsingh.customersupport.utility;

import android.content.Context;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * QueryBuilder Class for web API
 * Created by Kunal Singh
 * Date 01 January 2017
 */
public class QueryBuilder {

    private final String TAG = "QueryBuilder";
    private static QueryBuilder instance;

     public static QueryBuilder getInstance() {
        if (instance == null)
            instance = new QueryBuilder();

        return instance;
    }

    public String login() {
        StringBuilder sb = new StringBuilder(Settings.LOGIN_API);
        //sb.append("login");
        Logger.logInfo(TAG, "login", " " + sb.toString());
        return sb.toString();
    }

    public JSONObject generateLoginQuery(Context context, String userName, String password, String appVesion) {
        JSONObject jUser = new JSONObject();
        JSONObject jInput = new JSONObject();
        try {
            //JSONObject jInput = new JSONObject();
            jInput.put("username", userName.trim());
            jInput.put("password", password.trim());
            jInput.put("appVersion", appVesion.trim());
            jInput.put("osName", "Android");
            //jUser.put("",jInput);
            //jUser.put("input", jInput);
            Logger.logInfo(TAG, "login json Data", " " + jInput.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jInput;
    }
}
