package com.hitech.kunalsingh.customersupport.home;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.hitech.kunalsingh.customersupport.R;
import com.hitech.kunalsingh.customersupport.common_ui.HiTechButton;
import com.hitech.kunalsingh.customersupport.common_ui.HiTechEditText;
import com.hitech.kunalsingh.customersupport.fragment_classes.HomeFragment;
import com.hitech.kunalsingh.customersupport.fragment_drawer.MainActivity;

/**
 * Created by kunal.singh1 on 18-01-2017.
 */
public class RegisterScreenActivity extends AppCompatActivity implements View.OnClickListener {
    HiTechEditText edtTxtUserName, edtTxtUserCategiry, edtTxtUserMobile,edtTxtUserEmail, edtTxtEstablishmentName, edtTxtCity, edtTxtAddress;
    HiTechButton btnRegister;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_new_user);
        initialisation();
        setListener();

    }


    private void initialisation() {
        edtTxtUserName = (HiTechEditText) findViewById(R.id.edtTxtUserName);
       // edtTxtUserCategiry = (HiTechEditText) findViewById(R.id.edtTxtUserCategiry);
        edtTxtUserMobile = (HiTechEditText) findViewById(R.id.edtTxtUserMobile);
        edtTxtUserEmail=(HiTechEditText) findViewById(R.id.edtTxtUserEmail);
        edtTxtEstablishmentName = (HiTechEditText) findViewById(R.id.edtTxtEstablishmentName);
        edtTxtCity = (HiTechEditText) findViewById(R.id.edtTxtCity);
        edtTxtAddress = (HiTechEditText) findViewById(R.id.edtTxtCity);
        btnRegister = (HiTechButton) findViewById(R.id.btnRegister);
    }

    private void setListener() {
        btnRegister.setOnClickListener(this);
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnRegister:
               /* if(edtTxtUserCategiry.getText().toString().equalsIgnoreCase("")){
                    edtTxtUserCategiry.setError("This field is mandatory");
                    edtTxtUserCategiry.requestFocus();
                }else*/ if(edtTxtUserName.getText().toString().equalsIgnoreCase("")) {
                    edtTxtUserName.setError("This field is mandatory");
                    edtTxtUserName.requestFocus();
                }else if(edtTxtUserMobile.getText().toString().equalsIgnoreCase("")) {
                    edtTxtUserMobile.setError("This field is mandatory");
                    edtTxtUserMobile.requestFocus();
                }else if(edtTxtCity.getText().toString().equalsIgnoreCase("")) {
                    edtTxtCity.setError("This field is mandatory");
                    edtTxtCity.requestFocus();
                }else{
                    startActivity(new Intent(RegisterScreenActivity.this, MainActivity.class));
                    finishAffinity();
                }

                break;
            default:
                break;
        }
    }
}
