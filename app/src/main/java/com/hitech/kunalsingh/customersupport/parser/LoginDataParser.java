package com.hitech.kunalsingh.customersupport.parser;

import android.provider.ContactsContract;
import android.support.v4.app.FragmentActivity;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.hitech.kunalsingh.customersupport.controller.BaseController;
import com.hitech.kunalsingh.customersupport.utility.Settings;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * LoginDataParser class
 * Created by Kunal Singh
 * Date 23 January 2017
 */
public class LoginDataParser extends BaseController {

    private onLoginListener listener;
    private int id;
    private String TAG_MESSAGE = "message";

    public LoginDataParser executeQuery(FragmentActivity context, String url, JSONObject json, onLoginListener listener, int id, boolean toShowDialog) {
        // RequestManager.allowAllSSL();
        this.listener = listener;
        this.id = id;
        init(context, url, Request.Method.POST, json,toShowDialog);

        return null;
    }

    @Override
    public void onComplete(JSONArray response, String message) {
        switch (id) {
            case Settings.LOGIN_ID:

                listener.onSuccess(id, message, null);
                /*if (response != null)
                    if (message != null)
                        listener.onSuccess(id, message, null);*/
                   // listener.onSuccess(id, "", parserUserInfo(response.getJSONObject(0)));
                break;

        }
    }

    @Override
    public void onSuccess(JSONArray response) {

    }


    @Override
    public void onFailure(String error,ErrorCode code) {
        if (error != null) {
            listener.onError(error, id,code);
        }
    }

    public interface onLoginListener {
        public   void onSuccess(int id, String message, Object object);
        public  void onError(String error, int id,ErrorCode code);
    }


    private LoginData parserUserInfo(JSONObject object) {

        Gson gson = new Gson();
        LoginData response = gson.fromJson(object.toString(), LoginData.class);
        return response;
    }
}