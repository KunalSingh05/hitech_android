package com.hitech.kunalsingh.customersupport.utility;

import android.content.Context;
import android.net.ConnectivityManager;
import android.view.View;
import android.widget.Toast;

import com.hitech.kunalsingh.customersupport.R;

/**
 * Utils class for common method
 * Created by Kunal Singh
 * Date 29 December 2016
 */
public class Utils {
    static Toast toast1;
    private static String TAG = "Utils";

    public static void HitechToast(Context context, String msg) {


        toast1 = Toast.makeText(context, " ", Toast.LENGTH_SHORT);
        View toastView = toast1.getView();
        toastView.setBackgroundResource(R.drawable.toast_drawable);
        toast1.setText(msg);
        toast1.show();
    }

    public static boolean isNetworkAvailable(Context context) {
        boolean isNetworkAvailable = false;
        ConnectivityManager manager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        // For 3G check
        boolean is3g = false;
        if (manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE) != null)
            is3g = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                    .isConnectedOrConnecting();
        // For WiFi Check
        boolean isWifi = false;
        if (manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI) != null)
            isWifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                    .isConnectedOrConnecting();

        if (is3g || isWifi) {
            isNetworkAvailable = true;
        }

        return isNetworkAvailable;
    }
}
