package com.hitech.kunalsingh.customersupport.common_ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Window;

import com.hitech.kunalsingh.customersupport.R;
import com.victor.loading.rotate.RotateLoading;

/**
 * Common Progress Dialog
 * Created by Kunal Singh
 * Date 01 January 2017
 */
public class HiTechProgressDialog extends Dialog {

    private RotateLoading rotateLoading;
    public HiTechProgressDialog(Context a) {
        super(a);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setBackgroundDrawable(new ColorDrawable(0));
        setContentView(R.layout.custom_progress_dialog);
      /*  AVLoadingIndicatorView loader=(AVLoadingIndicatorView)findViewById(R.id.avi);
        loader.show();*/
        rotateLoading = (RotateLoading) findViewById(R.id.loading_spinner);
        rotateLoading.start();



    }
}

