package com.hitech.kunalsingh.customersupport.parser;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by kunal.singh1 on 23-01-2017.
 */
public class LoginData implements Parcelable {

    @SerializedName("name")
    public String name;

    @SerializedName("mobileNumber")
    public String mobileNumber;

    protected LoginData(Parcel in) {
        name = in.readString();
        mobileNumber = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(mobileNumber);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<LoginData> CREATOR = new Creator<LoginData>() {
        @Override
        public LoginData createFromParcel(Parcel in) {
            return new LoginData(in);
        }

        @Override
        public LoginData[] newArray(int size) {
            return new LoginData[size];
        }
    };
}