package com.hitech.kunalsingh.customersupport.controller;

import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.Window;
import android.view.WindowManager;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by Kunal Singh
 * Date 04 January 2017
 */
public class BaseActivity  extends AppCompatActivity {


    public AppCompatActivity activity;
    public void init(AppCompatActivity activity,String name)
    {

        this.activity=activity;
        AppController application = (AppController) getApplication();
        Tracker mTracker = application.getDefaultTracker();
        mTracker.setScreenName(name);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());

    }

}
