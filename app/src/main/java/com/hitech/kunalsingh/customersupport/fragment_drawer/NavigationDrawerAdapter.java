package com.hitech.kunalsingh.customersupport.fragment_drawer;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;


import com.hitech.kunalsingh.customersupport.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by Kunal Singh on 11/16/2015.
 */
public class NavigationDrawerAdapter extends RecyclerView.Adapter<NavigationDrawerAdapter.MyViewHolder>{
    List<NavDrawerItem> data = Collections.emptyList();
    private LayoutInflater inflater;
    private Context context;
    public static int selected_item = 0;
    Typeface prestigefont;
    public NavigationDrawerAdapter(Context context, List<NavDrawerItem> data) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    public void delete(int position) {
        data.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.nav_drawer_row, parent, false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final NavDrawerItem current = data.get(position);
        holder.title.setText(current.getTitle());
        holder.drawerIcon.setImageResource(current.getIcon());
        /**
         *  For Navigation Item colour and background
         */
        if(position == selected_item)
        {

            holder.title.setTextColor(Color.parseColor("#FFFFFF"));
            holder.drawerIcon.setImageResource(current.getSelIcon());
            holder.title.setPaintFlags(holder.title.getPaintFlags()| Paint.UNDERLINE_TEXT_FLAG);
            holder.title.setTypeface(prestigefont);
            holder.root.setBackgroundResource(R.drawable.list_item_bg_normal);
        }
        else
        {
            holder.title.setTextColor(Color.parseColor("#004D40"));
            holder.root.setBackgroundResource(R.drawable.list_item_bg_pressed);
            holder.title.setPaintFlags(holder.title.getPaintFlags() & (~ Paint.UNDERLINE_TEXT_FLAG));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView drawerIcon;

        RelativeLayout root;


        public MyViewHolder(View itemView) {
            super(itemView);
            String path_prestige = "font/prestige.ttf";
            title = (TextView) itemView.findViewById(R.id.title);
            Typeface prestigefont = Typeface.createFromAsset(itemView.getContext().getAssets(), path_prestige);
            drawerIcon= (ImageView) itemView.findViewById(R.id.drawerIcon);
            root= (RelativeLayout) itemView.findViewById(R.id.rly_nav_icon);
        }
    }
}

