package com.hitech.kunalsingh.customersupport.utility;



/**
 * Settings class
 * Created by Kunal Singh
 * Date 29 December 2016
 */
public class Settings {
    public static final int SPLASH_TIME_OUT = 2000;
    public static final String ABOUT_HGP="http://59.160.98.142/Hero/about_hgp.html";
    public static final String COMPANY_PROFILE="http://59.160.98.142/Hero/company_profile.html";
    public static final String PART_LOCATOR="https://mobileapps.heromotocorp.com/part_locator/";

    public static final String API_SERVER = "http://59.160.98.142:8085/HitechService";
    public static final String LOGIN_API = API_SERVER + "/auth";
    //web services calling id
    public static final int LOGIN_ID = 1;

}
